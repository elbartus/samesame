use anyhow::{anyhow, Result};
use std::fs::File;
use std::io::{self, BufRead};
use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "samesame", about = "Match lines from one file in another file.")]
pub struct Opt {
    /// Shows line information from file1 and file2 for each match.
    /// Doesn't do anything, if --quiet is also set.
    #[structopt(short, long)]
    pub verbose: bool,

    /// Only matches if lines in file1 and file2 are exactly the same
    /// otherwise searches in the whole line of file2.
    #[structopt(short, long)]
    pub exact: bool,

    /// Only returns exit code 0, if all lines of file1 are found in file2.
    #[structopt(short, long)]
    pub all: bool,

    /// If true, does not print out anything on stdout.
    #[structopt(short, long)]
    pub quiet: bool,

    /// Prints out a report in the end. Doesn't do anything, if --quiet is also set.
    #[structopt(short, long)]
    pub report: bool,

    /// If true, print's out the lines of file1, which didn't have a match in file2.
    #[structopt(short, long)]
    pub invert: bool,

    /// Path to file 1
    #[structopt(parse(from_os_str))]
    pub file1: PathBuf,

    /// Path to file 2
    #[structopt(parse(from_os_str))]
    pub file2: PathBuf,
}

fn read_lines(path: &PathBuf) -> Result<io::Lines<io::BufReader<File>>> {
    let file = File::open(path)?;
    Ok(io::BufReader::new(file).lines())
}

fn print_match_opt_aware(opt: &Opt, idx1: usize, idx2: usize, line1: &String, line2: &String) {
    if opt.invert {
        return;
    }

    if !opt.quiet {
        if opt.verbose {
            println!("{}\t{}\t{}\t{}", idx1 + 1, line1, idx2 + 1, line2);
        } else {
            println!("{}", line1);
        }
    }
}

fn print_no_match_opt_aware(opt: &Opt, idx1: usize, line1: &String) {
    if !opt.invert {
        return;
    }

    if !opt.quiet {
        if opt.verbose {
            println!("{}\t{}", idx1, line1);
        } else {
            println!("{}", line1);
        }
    }
}

fn match_type(exact: bool, l1: &String, l2: &String) -> bool {
    if exact {
        *l1 == *l2
    } else {
        l2.contains(l1)
    }
}

fn print_report(
    opt: &Opt,
    matches_1: usize,
    line_count_file1: usize,
    matches_2: usize,
) -> Result<()> {
    let found_all = matches_1 == line_count_file1;

    if opt.quiet {
        if !found_all && opt.all {
            return Err(anyhow!(
                "Did not match {} lines of file1 (total lines: {}) in file2.",
                line_count_file1 - matches_1,
                line_count_file1
            ));
        }
        return Ok(());
    }

    if opt.invert {
        if opt.report {
            println!(
                "\nDid not match {} lines of file1 (total lines: {}) in file2.",
                line_count_file1 - matches_1,
                line_count_file1
            );
        }
    } else {
        if found_all {
            if opt.report {
                println!(
                    "\nFound matches for all lines ({}) of file1 in file2.",
                    matches_1
                );
            }
        } else {
            if opt.all {
                return Err(anyhow!(
                    "Did not match {} lines of file1 (total lines: {}) in file2.",
                    line_count_file1 - matches_1,
                    line_count_file1
                ));
            }
            if opt.report {
                println!(
                    "\nFound {} matches in file2 from {} lines in file1.\n{} lines from file1 did not match lines in file2.",
                    matches_2, matches_1, line_count_file1 - matches_1
                );
            }
        }
    }

    Ok(())
}

fn main() -> Result<()> {
    let opt = Opt::from_args();
    if !opt.file1.exists() {
        return Err(anyhow!(
            "file1 does not exist: {}.",
            opt.file1.to_str().unwrap_or("unknown file path")
        ));
    }

    if !opt.file2.exists() {
        return Err(anyhow!(
            "file2 does not exist: {}.",
            opt.file2.to_str().unwrap_or("unknown file path")
        ));
    }

    let mut lines2 = Vec::new();
    let mut matches_1 = 0_usize;
    let mut matches_2 = 0_usize;
    let mut line_count_file1 = 0_usize;

    read_lines(&opt.file2)
        .unwrap()
        .into_iter()
        .for_each(|line| {
            if let Ok(l) = line {
                lines2.push(l);
            }
        });

    read_lines(&opt.file1)
        .unwrap()
        .into_iter()
        .enumerate()
        .for_each(|(idx1, line)| {
            if let Ok(ref l1) = line {
                line_count_file1 = idx1 + 1;
                let mut matched = false;
                lines2.iter().enumerate().for_each(|(idx2, l2)| {
                    if match_type(opt.exact, l1, l2) {
                        matched = true;
                        matches_2 += 1;
                        print_match_opt_aware(&opt, idx1, idx2, l1, l2);
                    }
                });

                if matched {
                    matches_1 += 1;
                } else {
                    print_no_match_opt_aware(&opt, idx1, l1);
                }
            }
        });

    print_report(&opt, matches_1, line_count_file1, matches_2)
}
