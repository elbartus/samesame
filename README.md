# samesame
`samesame` lets you match lines from one file in another file!

# Installation
Be sure to have cargo installed on your machine and the cargo 
binary path added to your shells `$PATH` variable.

Installation is then as easy as:
```bash
cargo install samesame
```

# Examples
- Match lines from var/1 in var/2
  ```bash
  samesame var/1 var/2
  ```
- Show a report
  ```bash
  samesame --report var/1 var/2
  ```
- Show lines of file1/file2 for matches
  ```bash
  samesame --verbose var/1 var/2  
  ```
- Require all lines of file1 to be found in file2. Otherwise the program exits with code 1.
  ```bash
  samesame --all var/1 var/2  
  ```
- Show lines of file1, which did **not** match in file2
  ```bash
  samesame --invert var/1 var/2
  ```

# Documentation
```
samesame 0.1.0
Match lines from one file in another file.

USAGE:
    samesame [FLAGS] <file1> <file2>

FLAGS:
    -a, --all        Only returns exit code 0, if all lines of file1 are found in file2
    -e, --exact      Only matches if lines in file1 and file2 are exactly the same otherwise searches in the whole line
                     of file2
    -h, --help       Prints help information
    -i, --invert     If true, print's out the lines of file1, which didn't have a match in file2
    -q, --quiet      If true, does not print out anything on stdout
    -r, --report     Prints out a report in the end. Doesn't do anything, if --quiet is also set
    -V, --version    Prints version information
    -v, --verbose    Shows line information from file1 and file2 for each match. Doesn't do anything, if --quiet is also
                     set

ARGS:
    <file1>    Path to file 1
    <file2>    Path to file 2
```
